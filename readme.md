# Команды

## Команды Docker

Создать проект:  
`docker init`

Список контейнеров:  
`docker ps`

Собрать билд:  
`docker build -t build_name .`

Спулить образ/имедж:  
`docker pull image_name`

Запустить образ/имедж (контейнер будет иметь имя **container_name**):  
`docker run --name container_name image_name`

## Команды docker compose

Запустить композ и сбилдить:  
`docker compose up -d --build`
